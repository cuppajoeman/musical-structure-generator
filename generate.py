import random
from typing import Tuple, Set

def horizontal_flip(structure: Tuple[int])  -> Tuple[int]:
  """
  Given a structure return a new structure which is flipped across the y axis

  Remark: this function is an involution
  """
  return tuple(reversed(structure))

def vertical_flip(structure: Tuple[int])  -> Tuple[int]:
  """
  Given a structure return a new structure which is flipped across the x axis

  Remark: this function is an involution
  """
  structure_length = len(structure)
  return tuple((structure_length - 1) - v for v in structure)

def combine_structures(structure_a: Tuple[int], structure_b: Tuple[int], separator=False) -> Tuple[int]:
  """
  Given two structures, combine them together into a new structure and return it
  """
  combined_structure = []
  for structure in [structure_a, structure_b]:
    for value in structure:
      combined_structure.append(value)
    if separator:
      combined_structure.append(-1)
  return tuple(combined_structure)
  

def generate_all_structures(base_structures: Set[Tuple[int]]) -> Set[Tuple[int]]:
  """
  Given set of base structures, generate all possible structures using the rules
  """
  # the following variable chooses which functions to apply
  all_function_choices = [(x,y) for x in [True, False] for y in [True, False]]
  all_structures = set()
  for base_structure in base_structures:
    for function_choice in all_function_choices:
      doing_horizontal_flip = function_choice[0]
      doing_vertical_flip = function_choice[1]
      new_structure = base_structure
      if doing_horizontal_flip:
        new_structure = horizontal_flip(new_structure)
      if doing_vertical_flip:
        new_structure = vertical_flip(new_structure)
      all_structures.add(new_structure)
  return all_structures

def choose_n_random_structures_and_combine(structures: Set[Tuple[int]], n: int, separator=False) -> Set[Tuple[int]]:
  """
  Given a set of structures choose n of them randomly with replacement and combine them
  into a larger structure

  Assumptions: structures is
  """
  n_structures = []
  for _ in range(n):
    n_structures.append(random.sample(structures, 1)[0])
  
  combined_structure = n_structures.pop()

  while len(n_structures) != 0:
    new_structure = n_structures.pop()
    combined_structure = combine_structures(combined_structure, new_structure, separator)
  
  return combined_structure
