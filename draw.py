GAP_SIZE = 3
STRUCTURE_HEIGHT = 4

from typing import Tuple, Set
def draw_multiple_structures_new_line(structures: Set[Tuple[int]]): 
  """
  Given a list of structures print them on new lines
  """
  for structure in structures:
    print(structure)
    draw_structure(structure)

def draw_structure(structure: Tuple[int]):
  """
  Given a list of integers draw the visual representation of that structure
  """
  structure_length = len(structure)

  # we double the x size because a character is almost twice as tall as it is wide
  blank_grid = [[" " for x in range(GAP_SIZE * structure_length)] for y in range(STRUCTURE_HEIGHT)] 

  for i, val in enumerate(structure):
    
    separator = val == -1
    if not separator:
      height = val
      inverted_height = (STRUCTURE_HEIGHT - 1) - height
      blank_grid[inverted_height][i * GAP_SIZE] = "x"
    else:
      for h in range(STRUCTURE_HEIGHT):
        blank_grid[h][i * GAP_SIZE] = "|"

  filled_grid = blank_grid

  string_repr = ""
  for row in filled_grid:
    string_repr += ("".join(row) + "\n")

  print(string_repr)
