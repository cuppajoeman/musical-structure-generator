import constants, generate, draw

all_structures = generate.generate_all_structures(constants.BASE_STRUCTURES)
#print(len(all_structures))
#draw.draw_multiple_structures_new_line(all_structures)

draw.draw_structure(generate.choose_n_random_structures_and_combine(all_structures, 4, True))
